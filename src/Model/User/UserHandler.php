<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\User;

use App\Entity\User;

class UserHandler
{
    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data)
    {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $password = $this->encodePassword($data['password']);
        $user->setPassword($password);

        return $user;
    }

    /**
     * @param array $data
     * @return User
     */
    public function createNewSocUser(array $data)
    {
        $user = new User();
        $user->setOriginalCity($data['original_city']);
        $user->setLastName($data['last_name']);
        $user->setEmail($data['email']);
        $user->setFirstName($data['first_name']);
        $user->setIdentity($data['identity']);
        $user->setVerifiedEmail($data['verified_email']);
        $user->setProfileUrl($data['profile']);
        $user->setUid($data['uid']);
        $user->setNetwork($data['network']);
        $password = $this->encodePassword(rand(1,1000));
        $user->setPassword($password);

        return $user;
    }

    public function encodePassword($password)
    {
        return md5($password).md5($password.'2');
    }
}
