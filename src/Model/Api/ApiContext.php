<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_ONE_CLIENT_DATA = '/get-one-data-client/{password}/{email}';
    const ENDPOINT_ONE_SOC_CLIENT_DATA = '/get-one-soc-data-client/{uid}/{email}';
    const ENDPOINT_CREATE_CLIENT_SOC = '/create-client-soc';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
                'passport' => $passport,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @param string $email
     * @return mixed
     * @internal param string $passport
     * @internal param array $data
     */
    public function getOneDataClient(string $password, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ONE_CLIENT_DATA, [
            'password' => $password,
            'email' => $email
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $uid
     * @param string $email
     * @return mixed
     * @internal param string $password
     * @internal param string $passport
     * @internal param array $data
     */
    public function getOneSocDataClient(string $uid, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_ONE_SOC_CLIENT_DATA, [
            'uid' => $uid,
            'email' => $email
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClientSoc(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_CLIENT_SOC, self::METHOD_POST, $data);
    }
}
