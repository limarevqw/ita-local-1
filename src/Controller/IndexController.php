<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function indexAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/profile/authorization-successful", name="authorization_successful")
     * @return Response
     */
    public function authorizationSuccessfulAction()
    {
        return $this->render('authorization_successful.html.twig',[

        ]);
    }

    /**
     * @Route("/auth", name="auth")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return Response
     * @internal param UserHandler $handler
     */
    public function authAction(
        ApiContext $apiContext,
        Request $request,
        UserRepository $userRepository,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        $error = null;
        $form = $this->createForm(LoginType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();

                $password = $data['password'];
                $email = $data['email'];

                $user = $userRepository->findOneByPasswordAndEmail($password, $email);

                if ($user) {
                    $this->signIn($user);
                    return $this->redirectToRoute("authorization_successful");
                } else {

                    $client = $apiContext->getOneDataClient($userHandler->encodePassword($password), $email);

                    if ($client['status']) {

                        $newUser = $userHandler->createNewUser($client['data']);

                        $manager->persist($newUser);
                        $manager->flush();

                        $user = $userRepository->findOneByPasswordAndEmail($password, $email);

                        $this->signIn($user);
                        return $this->redirectToRoute("authorization_successful");

                    } else {
                        $error = 'Error: Пользователь не найден!. Вам нужно зарегистрироватся.';
                    }
                }

            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('auth.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/social-auth-back", name="social_auth_back")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return Response
     */
    public function socialAuthBackAction(
        Request $request,
        UserRepository $userRepository,
        ApiContext $apiContext,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        if ($request->isMethod("POST")) {
            $host = $request->getHost();
            $token = $request->get("token");
            $s = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $host);
            $userDataSoc = json_decode($s, true);

            if ($userDataSoc) {
                try {

                    $data['original_city'] = @$userDataSoc['original_city'];
                    $data['last_name'] = @$userDataSoc['last_name'];
                    $data['email'] = @$userDataSoc['email'];
                    $data['first_name'] = $userDataSoc['first_name'];
                    $data['identity'] = @$userDataSoc['identity'];
                    $data['verified_email'] = @$userDataSoc['verified_email'];
                    $data['profile'] = @$userDataSoc['profile'];
                    $data['uid'] = @$userDataSoc['uid'];
                    $data['network'] = $userDataSoc['network'];

                    if (empty($data['email']) && empty($data['uid'])) {
                        throw new ApiException("Авторизация не возможна отсутствует email или uid");
                    }

                    $uid = $data['uid'];
                    $email = $data['email'];
                    $user = $userRepository->findOneByUidAndEmail($uid, $email);

                    if ($user) {
                       $this->signIn($user);
                       return $this->redirectToRoute("authorization_successful");
                    } else {
                       $client = $apiContext->getOneDataClient($uid, $email);

                       if ($client['status']) {
                           $newUser = $userHandler->createNewSocUser($client['data']);

                           $manager->persist($newUser);
                           $manager->flush();

                           $user = $userRepository->findOneByUidAndEmail($uid, $email);

                           $this->signIn($user);
                           return $this->redirectToRoute("authorization_successful");
                       }
                    }

                    $apiContext->createClientSoc($data);

                    $newUser = $userHandler->createNewSocUser($data);

                    $manager->persist($newUser);
                    $manager->flush();

                    $user = $userRepository->findOneByUidAndEmail($uid, $email);

                    $this->signIn($user);

                    return $this->redirectToRoute("authorization_successful");

                } catch (ApiException $e) {
                    $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
                    return new Response($error);
                }

            } else {
                return new Response('Error: Данные отсутствуют.');
            }

        } else {
            return new Response('Error: 404');
        }
    }

    public function signIn(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

}
